<?php

namespace App\Service;

use App\Utils\StringUtils;

class JWT
{
    // générer un JSON Web Token
    public function generate():String
    {
        /*
            étapes
                - encoder le header en base64URL
                - encoder le payload en base64URL
                - hacher le header et le payload et les associer à un secret
        */
        
        // header : configuration du JWT
        $header = [
            'alg' => 'HS256',
            'typ' => 'JWT',
        ];
        $headerBase64 = StringUtils::base64url_encode(json_encode($header));

        // payload (charge utile) : données à transporter non sécurisés
        // iat : timestamp de la date et de l'heure de génération du token

        $payload = ['iat' => time()];
        $payloadBase64 = StringUtils::base64url_encode(json_encode($payload));

        // création de la signature en utilisant la clé secrète
        $signatureBase64 = StringUtils::base64url_encode(hash_hmac('sha256',"$headerBase64.$payloadBase64",$_ENV['SECRET']));

        // création du jeton 
        $token = "$headerBase64.$payloadBase64.$signatureBase64";

        return $token;
    }

    public function verify(String $token):bool
    {
        // eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MTM2NTU5MTN9.MGQwOTQzZjQxZTNlOTczODhhZjE3OWI1NDdjZTUyYTM3OWU5ZjgxOTE1ZjhkMmI3MzFkY2MyYzRlNWVkMGY2Yg

        // séparer le header, du payload de la signature
        [ $headerBase64, $payloadBase64, $signature ] = explode('.', $token);

        // signature
        $signatureBase64 = StringUtils::base64url_encode(hash_hmac('sha256',"$headerBase64.$payloadBase64",$_ENV['SECRET']));

        // vérification de la signature 
        // tester si la nouvelle signature est identique à la signature contenue dans le token
        $signatureVerify = $signatureBase64 == $signature ? true : false;

        return $signatureVerify;
    }
}
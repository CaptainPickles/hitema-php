<?php 

/*
    App : dossier src, dossier contenant toutes les classes de l'application, spécifié dans le composer.json
    Core : dossier src/Core
*/

namespace App\Core;

/*
    le Routage doit récupérer l'URL et trouver le contrôleur et la méthode, reliés à la route (l'URL)
*/

class Routing
{
    /*
        lister toutes les routes de l'application 
            clé : la route
            valeur : array contenent le nom du contrôleur et le nom de la méthode
    */
    private array $routes = [
        '/' => [
            'controller' => 'Homepage',
            'method' => 'index',
        ],
        '/auth' => [
            'controller' => 'Authentification',
            'method' => 'index',
        ],
        /*
            utilisation d'une expression rationnelle
                () : création d'un groupe
                \d : chiffre
                + : 1 ou plusieurs
                ?<_> : nommer le groupe
        */
        /*
        '/(?<id>\d+)' => [
            'controller' => 'Homepage',
            'method' => 'index',
        ],
        */
    ];

    // retourner le contrôleur et la méthode 
    public function getRouteInfos():array
    {
        // récupérer la route (URL)
        $uri = $_SERVER['REQUEST_URI'];

        // valeur par défaut
        $result = [
            'controller' => 'NotFound',
            'method' => 'index',
            'vars' => [],
        ];

        // tester si la route est listée
        foreach($this->routes as $regexp => $infos){
            /*
                preg_match : tester la concordance avec une expression rationnelle
                expression rationnelle, 3 paramètres 
                    - expression rationnelle 
                    - chaine à caractères à tester
                    - récupération des groupes  
            */
            if(preg_match("#^$regexp$#", $uri, $groups))
            {
                $result = $infos;
                $result['vars'] = $groups;

                // stopper la boucle
                break;
            }
        }
        /*
        if(array_key_exists($uri, $this->routes)){
            $result = $this->routes[$uri];
        } 
        */

        // dans les groupes des expressions rationnelles, ne conserver que les clés non numérique
        foreach($result["vars"] as $key => $value)
        {
            //unset : supprimer une entrée dans un array
            if(is_int($key))
            {
                unset($result["vars"][$key]);
            }
        }
        return $result;
    }
}
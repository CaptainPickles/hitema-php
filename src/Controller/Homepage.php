<?php

namespace App\Controller;

// Si le fichier n'est pas dans le même dossier use App\Controller\AbstractController;

class Homepage extends AbstractController
{
    /*
    public function index(array $data = []):void
    {
        $this->render("homepage/index",[
            "message" => $data["id"],
        ]);
    }
    */
    public function index(array $data = []):void
    {
        $this->render("homepage/index");
    }
}